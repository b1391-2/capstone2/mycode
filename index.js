
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 3000;
const cors = require("cors");

//Get useroutes
const userRoutes = require("./routes/UserRoutes");
const productRoutes = require("./routes/ProductRoutes");
const orderRoutes = require("./routes/OrderRoutes");

mongoose.connect("mongodb+srv://admin:admin@cluster0.qtkm7.mongodb.net/ecommerce?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));


/*Middlewares*/
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors());

app.use("/ecommerce/users", userRoutes);
app.use("/ecommerce/products", productRoutes);
app.use("/ecommerce/orders", orderRoutes);


app.listen(PORT, () => console.log(`Server running at port ${PORT}`))
