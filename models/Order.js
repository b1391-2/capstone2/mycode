const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
	{
		totalAmount: {
			type: Number
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		userId: {
			type: String,
			required: [true, "User ID is required"]
		},
		isFulfilled: {
			type: Boolean,
			default: false
		},
		orderedProducts: [
			{
				productId: {
					type: String
				}
			}
		]
	}
);


//exporting the model

module.exports = mongoose.model("Order", orderSchema);