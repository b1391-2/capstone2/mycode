const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		productName: {
			type: String,
			required: [true, "Product name is required"]
		},
		productDescription: {
			type: String,
			required: [true, "Product description is required"]
		},
		productPrice: {
			type: Number,
			required: [true, "Price is required"]
		},
		isActive: {
			type: Boolean,
			default: false
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	}
);


//exporting the model

module.exports = mongoose.model("Product", productSchema);