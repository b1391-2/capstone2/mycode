//Mongoose Module
const mongoose = require("mongoose");

//Schema

const userSchema = new mongoose.Schema(
	{
		firstName: {
		type: String,
		required: [true, "First name is required"]
		},
		lastName: {
			type: String,
			required: [true, "Last name is required"]
		},
		mobileNo: {
			type: String,
			required: [true, "Mobile number is required"]
		},
		email: {
			type: String,
			required: [true, "Email is required"]
		},
		password: {
			type: String,
			required: ["Password is required"]
		},
		isAdmin: {
			type: Boolean,
			default: false
		}
	}
);

//Export the model

module.exports = mongoose.model("User", userSchema);