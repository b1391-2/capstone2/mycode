//Importing the User Model
const User = require("./../models/User");

//Importing Order Model
const Order = require("./../models/Order");

//Requiring Bcrypt to hash password
const bcrypt = require("bcrypt");

//Calling Auth to decode and decode webtoken
const auth = require("./../auth");


//Register a user
module.exports.register = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		mobileNo : reqBody.mobileNo,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((result, error) => {
		if (result) {
			return true
		} else {
			return false
		}
	})
}

//Check if email exists
module.exports.checkEmail = (reqBody) => {
	const {email} = reqBody
	return User.findOne({email: email}).then((result, error) => {
		if (result != null) {
			return false
		} else {
			return true
		}
	})
}

//Log In
module.exports.logIn = (reqBody) => {
	//destructure login
	const {email, password} = reqBody;

	return User.findOne({email :email}).then((result, error) => {
		//find email
		if (result == null) {
			return false 
		} else {
			let isPasswordCorrect = bcrypt.compareSync(password, result.password);
			if (isPasswordCorrect == true) {
				return {access: auth.createAccessToken(result)};
			} else {
				return false
			}
		}
	})
}

//Retrieve All Users

module.exports.allUsers = (data) => {
	const {isAdmin} = data

	return User.find().then((result, error) => {
		if (isAdmin == true) {
			return result
		} else {
			return error
		}
	})
}


//Retrieve profile

module.exports.getProfile = (data) => {
	//console.log(data);
	const {id, firstName, lastName, mobileNo} = data;

	return User.findOne({_id : id}).then((result, error) => {
		console.log(result);

		result = {
			firstName : result.firstName,
			lastName : result.lastName,
			mobileNo : result.mobileNo,
			password : "",
			isAdmin : result.isAdmin,
			email : result.email,
			_id : result.id
		}
		return result
	})
}

//Fetch user details by ID

module.exports.getUserProfile = (data, userId) => {
	const {isAdmin } = data
	//console.log(userId);
	return User.findById(userId).then((result, error) => {
		//console.log(result)
		result = {
			firstName : result.firstName,
			lastName : result.lastName,
			mobileNo : result.mobileNo,
			password : "",
			isAdmin : result.isAdmin,
			email : result.email,
			_id : result.id
		}

		if (isAdmin == true) {
			return result
		} else {
			return error
		}
	})
}

// Set User As Admin 

module.exports.setAsAdmin = (data, userId) => {
	const {id} = data
	let userStatus = {
		isAdmin : true
	}
	
	return User.findByIdAndUpdate(userId, userStatus, {new: true}).then((result,error) => {
		if (result == null) {
			return `User not exist`
		} else {
			if (result) {
				return result
			} else {
				return error
			}
		}
	})
}

//Make isAdmin  to false

module.exports.unsetAsAdmin = (data, userId) => {
	const {id} = data
	let userStatus = {
		isAdmin : false
	}
	
	return User.findByIdAndUpdate(userId, userStatus, {new: true}).then((result,error) => {
		if (result == null) {
			return `User not exist`
		} else {
			if (result) {
				return result
			} else {
				return error
			}
		}
	})
}