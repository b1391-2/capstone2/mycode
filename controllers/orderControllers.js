// Importing the models both user and product

const user = require("./../models/User");
const product = require("./../models/Product");
const order = require("./../models/Order");



//Creating a Cart first
module.exports.createOrder = (data) => {
	const {id} = data
	//console.log(id)
	let newOrder = new order({
		userId: id,
		totalAmount: 0
	})
	
	return newOrder.save().then((result, error) =>{
		if (error) {
			return false
		} else {
			return true
		}
	})
}

//Check if a cart under a user already exists

module.exports.cartExists = async (data) => {
	const {id, isAdmin} = data;

	let userID = {
		userId : id,
		isFulfilled: false
	}
	return order.findOne(userID).then((result, error) => {
		//console.log(result)
		
		if (result == null) {
			return true
		} 
		const { isFulfilled } = result 

		if (isFulfilled == false) {
			return false
		} else {
			return true
		}
	})

	
}

//Add to Cart

module.exports.addToCart = async (data, productId) => {
 	const {id, isAdmin} = data
 	//console.log(id);
 	let addedProducts
 	const userForOrder = {
 		userId: id,
 		isFulfilled: false
 	}

 	//console.log(user);
	const findUser = await user.findById(id).then((result, error) => {
		//console.log(result)
		if (error){
			return error
		} else {
			return result
		}
	})

	const findProduct = await product.findById(productId).then((result, error) => {
		//console.log(result)
		if (error) {
			return error
		} else {
			return result
		}
	})

	const retrieveCart = await order.findOne(userForOrder).then((result, error) => {
		const { productPrice } = findProduct

		let addedProduct = {
			productId : productId
		}

		addedProducts = result.orderedProducts.push(addedProduct);
		
		result.save().then((result, response) => {
			let { totalAmount , _id } = result;
			
			let totalPrice = totalAmount += productPrice;
			
			let amountOfCart = {
				$set: {totalAmount: totalPrice}
			}

			order.findOneAndUpdate(userForOrder, amountOfCart).then((result, error) => {
				if (result) {
					return result
				} else {
					return error
				}
			})
		})
	})

	if (findUser && findProduct) {
		return true
	} else {
		return false
	}
}


//Retrieve the Cart
module.exports.retrieveCart = (data) => {
	const {id, isAdmin} = data

	let userOrder = {
		userId: id,
		isFulfilled: false
	}

	return order.findOne(userOrder).then((result, error) => {
		if (result) {
			return result
		} else {
			return error
		}
	})
}

//Remove to Cart
module.exports.removeToCart = async (data, productId, index) => {
	const {id, isAdmin} = data
	let removeProducts;

 	const userForOrder = {
 		userId: id
 	}

 	const findUser = await user.findById(id).then((result, error) => {
		
		if (error){
			return error
		} else {
			return result
		}
	})

	const findProduct = await product.findById(productId).then((result, error) => {
	
		const {_id, productName, productPrice} = result


		if (error) {
			return error
		} else {
			return result
		}
	})

	const retrieveCart = await order.findOne(userForOrder).then((result, error) => {
		const { productPrice } = findProduct

		let removeProduct = {
			productId : productId
		}

		removeProducts = result.orderedProducts.splice(index, 1);

		result.save().then((result, response) => {
			let { totalAmount , _id } = result;
			
			let totalPrice = totalAmount -= productPrice;
			
			let amountOfCart = {
				$set: {totalAmount: totalPrice}
			}

			order.findOneAndUpdate(userForOrder, amountOfCart).then((result, error) => {
				if (result) {
					return result
				} else {
					return error
				}
			})
		})
	})

	if (findUser && findProduct) {
		return true
	} else {
		return false
	}

		

}


//Checkout Order

module.exports.checkOut = (data, orderId) => {
	const {id, isAdmin} = data
	//console.log(orderId);

	let checkOutOrder = {
		isFulfilled: true,
		purchasedOn: new Date()
	}


	let userOrder = {
		userId: id,
		isFulfilled: false
	}

	return order.findOneAndUpdate(userOrder, checkOutOrder).then((result, error) => {
		//console.log(result);
		if (error) {
			return error
		} else {
			return result
		}
	})
}


//Retrieve All Check-out Order

module.exports.retrieveFulfilledOrder = (data) => {
	const {id, isAdmin} = data

	let findTrue = {
		isFulfilled: true
	}

	return order.find(findTrue).then((result, error) => {
		//console.log(result);
		if (error) {
			return error
		} else {
			if (isAdmin == true) {
				return result
			} else {
				return `Not Admin`
			}
		}
	})
}

//Retrieve all orders

module.exports.retrieveAllOrders = (data) => {
	const {id, isAdmin} = data

	return order.find().then((result, error) => {
		if (error) {
			return error
		} else {
			if (isAdmin == true) {
				return result
			} else {
				return `Not Admin`
			}
		}
	})
}