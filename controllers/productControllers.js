//Importing the Product Model

const product = require("./../models/Product");

//Calling Auth to decode jsonwebtoken
const auth = require("./../auth");

module.exports.register = (data, reqBody) => {
	const { productName, productDescription, productPrice} = reqBody
	const {userId, isAdmin} = data

	let newProduct = new product({
		productName : reqBody.productName,
		productDescription : reqBody.productDescription,
		productPrice : reqBody.productPrice
	})

	return newProduct.save().then((result, error) => {
		if (isAdmin == true) {
			return result
		} else {
			return error
			}
		})
}


//Unarchive a Product by Id

module.exports.unarchiveProduct = (data, productId, reqBody) => {
	const {userId, isAdmin} = data
	
	let productID = productId
	let queryproductName = reqBody.productName;
	let query = { productName : queryproductName};

	let productStatus = {
		isActive : true
	}

	//console.log(productID);
		return product.findByIdAndUpdate(productID, productStatus, {new: true}).then((result, error) => {
			//console.log(result)
		if (result == null) {
			return `Product Not Existing`
		} else {
			if (isAdmin == true) {
				return result
			} else {
				return `User not Admin`
			}
		}
	})


}

//Archive a Product by Id

module.exports.archiveProduct = (data, productId, reqBody) => {
	const {userId, isAdmin} = data
	let productID = productId
	let query = { productName : reqBody.productName}
	let productStatus = {
		isActive : false
	}
	return product.findByIdAndUpdate(productID, productStatus, {new: true}).then((result, error) => {
		if (result == null) {
			return `Product Not Existing`
		} else {
			if (isAdmin == true) {
				return result
			} else {
				return `User Not Admin`
			}
		}
	}) 
}


//Retrieve all active products

module.exports.retrieveActiveProducts = () => {
	return product.find({isActive: true}).then((result, error) => {
		
		if (result) {
			return result
		} else {
			return false
		}
	})
}

//Update a product

module.exports.updateProduct = (data, id, reqBody) => {
	//console.log(reqBody)
	//console.log(id)
	const { productName, productDescription, productPrice } = reqBody;
	const { userId, isAdmin} = data

	let updatedProduct = {
		productName : productName,
		productDescription : productDescription,
		productPrice : productPrice
	}

	return product.findByIdAndUpdate(id, updatedProduct, {new: true}).then((result, error) => {
		//console.log(result);
		if (error) {
			return error
		} else {
			if (isAdmin == true) {
				return result
			} else {
				return error
			}
		}
	})
}

//Retrieve single product thru ID

module.exports.retrieveProduct = (id) => {
	//console.log(id);
	return product.findById(id).then((result, error) => {
		//console.log(result)
		if (result) {
			return result
		} else {
			return error
		}
	})
}
//Retrieve all products

module.exports.retrieveAllProducts = (data) => {
	const {id, isAdmin} = data
	return product.find().then((result, error) => {
		if (error) {
			return error
		} else {
			if (isAdmin == true) {
				return result
			} else {
				return error
			}
		}
	})
}

//Delete a product by ID

module.exports.deleteProduct = (data, productId) => {
	const {userId, isAdmin} = data

	return product.findByIdAndDelete(productId).then((result, error) => {
		if (error) {
			return error
		} else {
			if (isAdmin == true) {
				return result
			} else {
				return error
			}
		}
	})
}