//require express
const express = require("express");
//require router
const router = express.Router();

//Importing controllers
const userController = require("./../controllers/userControllers");
const auth = require("./../auth");


//Register a User

router.post("/register", (request, response) => {
	userController.register(request.body).then(result => response.send(result))
});


//Check if email exists
router.post("/email-exists", (request, response) => {
	userController.checkEmail(request.body).then(result => response.send(result))
})


//Log in 

router.post("/login", (request, response) => {
	userController.logIn(request.body).then(result => response.send(result));
})

//Get Profile Details
router.get("/details", auth.verify ,(request, response) => {
	let userData = auth.decode(request.headers.authorization);
	userController.getProfile(userData).then(result => response.send(result));
})

//Fetch a users details
router.get("/:userId/details/", auth.verify, (request, response) => {
	let userData = auth.decode(request.headers.authorization);
	userController.getUserProfile(userData, request.params.userId).then(result => response.send(result));
})

//Fetch all Users
router.get("/", auth.verify, (request, response) => {
	let userData = auth.decode(request.headers.authorization);
	userController.allUsers(userData).then(result => response.send(result));
})

//Authenticate User Convert to Admin

router.put("/:userId/setToAdmin", auth.verify, (request, response) => {
	let userData = auth.decode(request.headers.authorization);
	userController.setAsAdmin(userData, request.params.userId).then(result => response.send(result));
})

//Authenticate User Convert to Useronly

router.put("/:userId/unsetToAdmin", auth.verify, (request, response) => {
	let userData = auth.decode(request.headers.authorization);
	userController.unsetAsAdmin(userData, request.params.userId).then(result => response.send(result));
})




module.exports = router;
