const express = require("express");
const router = express.Router();

const productController = require("./../controllers/productControllers");
const auth = require("./../auth");

//Register a product

router.post("/register", auth.verify ,(request, response) => {
	let data = auth.decode(request.headers.authorization);
	productController.register(data, request.body).then(result => response.send(result));
})

//Unarchive a product

router.put("/:productId/unarchive", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization);
	let productId = request.params.productId
	productController.unarchiveProduct(data, productId, request.body).then(result => response.send(result));
})

//archive a product

router.put("/:productId/archive", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization);
	let productId = request.params.productId	
	productController.archiveProduct(data, productId, request.body).then(result => response.send(result));
})

//Retrieve active products

router.get("/active-products", (request, response) => {
	productController.retrieveActiveProducts().then(result => response.send(result));
})

//Update Product

router.put("/:productId/update-product", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization);
	productController.updateProduct(data, request.params.productId, request.body).then(result => response.send(result));
})

//Retrieve Single Product thru ID

router.get("/:productId/retrieve-product", (request,response) => {
	productController.retrieveProduct(request.params.productId).then(result => response.send(result));
})

//Retrieve all products

router.get("/", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization);
	productController.retrieveAllProducts(data).then(result => response.send(result));
})

//Delete a product by ID

router.delete("/:productId/delete-product", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization);
	productController.deleteProduct(data, request.params.productId).then(result => response.send(result));
})

module.exports = router;