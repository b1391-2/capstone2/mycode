const express = require("express");
const router = express.Router();

const orderController = require("./../controllers/orderControllers");
const auth = require("./../auth");

// Create an Cart with an authenticated user
router.post("/create-order", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization)
	//console.log(data);
	orderController.createOrder(data).then(result => response.send(result));
})


//Add to Cart by ID by an authenticated user 
router.post("/:productId/add-to-cart", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization)
	//console.log(data);
	orderController.addToCart(data, request.params.productId).then(result => response.send(result));
})

//Remove an Item to the cart
router.post("/:productId/:index/remove-to-cart", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization)
	orderController.removeToCart(data, request.params.productId, request.params.index).then(result => response.send(result));
})

//Check if a cart under a user already exists
router.get("/cart-exists", auth.verify ,(request, response) =>{
	let data = auth.decode(request.headers.authorization)
	orderController.cartExists(data).then(result => response.send(result));
})

//Retrieve The Cart by the user
router.get("/retrieve-cart", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization)
	orderController.retrieveCart(data).then(result => response.send(result));
})

//Checkout
router.put("/:orderId/check-out", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization);
	orderController.checkOut(data, request.params.orderId).then(result => response.send(result));
})

//Retrieve fulfilled Order

router.get("/fulfilled-orders", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization);
	orderController.retrieveFulfilledOrder(data).then(result => response.send(result));
})

//Retrieve all Orders

router.get("/", auth.verify, (request, response) => {
	let data = auth.decode(request.headers.authorization);
	orderController.retrieveAllOrders(data).then(result => response.send(result));
})


module.exports = router;